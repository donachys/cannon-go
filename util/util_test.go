package util

import (
	"testing"

	tl "github.com/JoelOtter/termloop"
	"github.com/google/go-cmp/cmp"
	cg "gitlab.com/donachys/cannon-go/cannon-go"
)

func TestSha512(t *testing.T) {
	want := "6d2d3e1ae15e4bd11391b109ceef1ba3a946c3f0611289bf71c30a9836ff19575f2d5e273075272c58aac425b33ba73ea57d28b25d91acffb77812f5404ccb69"
	if h := Sha512Hash([]byte("amazing test")); h != want {
		t.Errorf("got %v want %v", h, want)
	}

}

func TestMaxUint64(t *testing.T) {
	cases := []struct {
		a, b, want uint64
	}{
		// Test a > b
		{
			a:    5,
			b:    3,
			want: 5,
		},
		// Test a < b
		{
			a:    2,
			b:    7,
			want: 7,
		},
		// Test a == b
		{
			a:    3,
			b:    3,
			want: 3,
		},
	}
	for _, c := range cases {
		if r := MaxUint64(c.a, c.b); r != c.want {
			t.Errorf("got %v want %v", r, c)
		}
	}
}

func TestMaxFloat32(t *testing.T) {
	cases := []struct {
		a, b, want float32
	}{
		// Test a > b
		{
			a:    5,
			b:    3,
			want: 5,
		},
		// Test a < b
		{
			a:    2,
			b:    7,
			want: 7,
		},
		// Test a == b
		{
			a:    3,
			b:    3,
			want: 3,
		},
	}
	for _, c := range cases {
		if r := MaxFloat32(c.a, c.b); r != c.want {
			t.Errorf("got %v want %v", r, c)
		}
	}
}

func TestClampFloat32(t *testing.T) {
	cases := []struct {
		gLower, gA, gUpper, want float32
	}{
		{
			gLower: 0,
			gA:     5,
			gUpper: 10,
			want:   5,
		},
		{
			gLower: 0,
			gA:     -5,
			gUpper: 10,
			want:   0,
		},
		{
			gLower: 0,
			gA:     15,
			gUpper: 10,
			want:   10,
		},
	}
	for _, c := range cases {
		if got := ClampFloat32(c.gLower, c.gA, c.gUpper); got != c.want {
			t.Errorf("%v != %v", got, c.want)
		}
	}
}
func TestMaxInt(t *testing.T) {
	cases := []struct {
		a, b, want int
	}{
		// Test a > b
		{
			a:    5,
			b:    3,
			want: 5,
		},
		// Test a < b
		{
			a:    2,
			b:    7,
			want: 7,
		},
		// Test a == b
		{
			a:    3,
			b:    3,
			want: 3,
		},
	}
	for _, c := range cases {
		if r := MaxInt(c.a, c.b); r != c.want {
			t.Errorf("got %v want %v", r, c)
		}
	}
}

func TestMinInt(t *testing.T) {
	cases := []struct {
		a, b, want int
	}{
		// Test a > b
		{
			a:    5,
			b:    3,
			want: 3,
		},
		// Test a < b
		{
			a:    2,
			b:    7,
			want: 2,
		},
		// Test a == b
		{
			a:    3,
			b:    3,
			want: 3,
		},
	}
	for _, c := range cases {
		if r := MinInt(c.a, c.b); r != c.want {
			t.Errorf("got %v want %v", r, c)
		}
	}
}

func TestSumInt(t *testing.T) {
	cases := []struct {
		given []int
		want  int
	}{
		{
			given: []int{5, 5},
			want:  10,
		},
		{
			given: []int{},
			want:  0,
		},
		{
			given: []int{3},
			want:  3,
		},
	}
	for _, c := range cases {
		if r := SumInt(c.given); r != c.want {
			t.Errorf("got %v want %v", r, c)
		}
	}
}

func TestTermLoopRect(t *testing.T) {
	cases := []struct {
		givenRect  *cg.Rect
		givenColor tl.Attr
		want       *tl.Rectangle
	}{
		{
			givenRect: &cg.Rect{
				TopLeft:  &cg.Point{X: 10, Y: 10},
				BotRight: &cg.Point{X: 20, Y: 20},
			},
			givenColor: tl.ColorBlue,
			want:       tl.NewRectangle(10, 10, 10, 10, tl.ColorBlue),
		},
	}
	for _, c := range cases {
		got := TermLoopRect(c.givenRect, c.givenColor)
		if diff := cmp.Diff(c.want, got, cmp.AllowUnexported(*c.want)); diff != "" {
			t.Errorf("%v != %v, differs: (-want +got)\n%s", got, c.want, diff)
		}
	}
}
