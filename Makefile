NAME=cannon-go
CLI-NAME=cli-client
SERVER-NAME=server

IMAGE_NAME=donachys/$(NAME)
TAG=0.0.0

clean:
	rm -rf bin
	rm cover.*
	rm *.log

cloc:
	gocloc --not-match-d="cannon-go" --exclude-ext=mod .

checkenv:
ifndef PACKAGE
	$(error PACKAGE must be defined)
endif

test:
	go test -timeout 45s -race -coverprofile=cover.out $$(go list -f '{{.ImportPath}}' ./...)
	go tool cover -func=cover.out

cover: checkenv
	go test -coverprofile cover.out ./$(PACKAGE)
	go tool cover -html cover.out -o cover.html
	rm cover.out
	open cover.html

fmt:
	go fmt $$(go list -f '{{.ImportPath}}' ./...)

vet:
	go vet $$(go list -f '{{.ImportPath}}' ./...)

proto:
	protoc -I cannon-go/ cannon-go/cannongo.proto --go_out=plugins=grpc:cannon-go

darwin: darwin-cli darwin-server

darwin-cli:
	env GOOS=darwin GOAARCH=amd64 go build -v -o $(CURDIR)/bin/darwin/amd64/$(NAME) ./cmd/$(CLI-NAME)

darwin-server:
	env GOOS=darwin GOAARCH=amd64 go build -v -o $(CURDIR)/bin/darwin/amd64/$(SERVER-NAME) ./cmd/$(SERVER-NAME)

linux: linux-server linux-cli

linux-cli:
	env GOOS=linux GOAARCH=amd64 go build -v -o $(CURDIR)/bin/linux/amd64/$(NAME) ./cmd/$(CLI-NAME)

linux-server:
	env GOOS=linux GOAARCH=amd64 go build -v -o $(CURDIR)/bin/linux/amd64/$(SERVER-NAME) ./cmd/$(SERVER-NAME)

windows: win-cli win-server

win-cli:
	env GOOS=windows GOAARCH=amd64 go build -v -o $(CURDIR)/bin/windows/amd64/$(NAME).exe ./cmd/$(CLI-NAME)

win-server:
	env GOOS=windows GOAARCH=amd64 go build -v -o $(CURDIR)/bin/windows/amd64/$(SERVER-NAME).exe ./cmd/$(SERVER-NAME)

docker:
	docker build -t $(IMAGE_NAME):$(TAG) .
	docker tag $(IMAGE_NAME):$(TAG) $(IMAGE_NAME):latest

build-all: proto darwin linux windows docker
