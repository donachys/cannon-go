package gamestore

import (
	"reflect"
	"testing"

	"gitlab.com/donachys/cannon-go/game"
)

func TestAddAndGetGame(t *testing.T) {
	game1 := game.NewCannonGame()
	game1.AddPlayer(game.NewCannonPlayer("test_name"))

	cases := []struct {
		gs        *CannonGameStore
		givenGame *game.CannonGame
		givenKey  string
		want      *game.CannonGame
		wantFound bool
	}{
		{
			gs:        GetCannonGameStore(),
			givenGame: game1,
			givenKey:  "test_id",
			want:      game1,
			wantFound: true,
		},
		{
			gs:        new(CannonGameStore),
			givenGame: game1,
			givenKey:  "test_id",
			want:      game1,
			wantFound: true,
		},
		{
			gs:        new(CannonGameStore),
			givenGame: nil,
			givenKey:  "",
			want:      nil,
			wantFound: false,
		},
	}
	for _, c := range cases {
		if c.givenGame != nil {
			c.gs.AddGame(c.givenKey, c.givenGame)
		}
		got, found := c.gs.GetGame(c.givenKey)
		if !reflect.DeepEqual(c.givenGame, got) || found != c.wantFound {
			t.Errorf("%v != %v found? %v != %v", got, c.want, found, c.wantFound)
		}
	}
}

func TestAddAndDelGame(t *testing.T) {
	game1 := game.NewCannonGame()
	game1.AddPlayer(game.NewCannonPlayer("test_name"))

	cases := []struct {
		gs        *CannonGameStore
		givenGame *game.CannonGame
		givenKey  string
		want      bool
	}{
		{
			gs:        GetCannonGameStore(),
			givenGame: game1,
			givenKey:  "test_id",
			want:      false,
		},
		{
			gs:        new(CannonGameStore),
			givenGame: game1,
			givenKey:  "test_id",
			want:      false,
		},
		{
			gs:        new(CannonGameStore),
			givenGame: nil,
			givenKey:  "",
			want:      false,
		},
	}
	for _, c := range cases {
		if c.givenGame != nil {
			c.gs.AddGame(c.givenKey, c.givenGame)
		}
		c.gs.DelGame(c.givenKey)
		_, found := c.gs.GetGame(c.givenKey)
		if found != c.want {
			t.Errorf("%v != %v key exists? %v", found, c.want, found)
		}
	}
}
