module gitlab.com/donachys/cannon-go

require (
	cloud.google.com/go v0.32.0 // indirect
	github.com/JoelOtter/termloop v0.0.0-20171105180545-99de0b20653e
	github.com/golang/lint v0.0.0-20181026193005-c67002cb31c3 // indirect
	github.com/golang/protobuf v1.2.0
	github.com/google/go-cmp v0.2.0
	github.com/mattn/go-runewidth v0.0.3 // indirect
	github.com/natefinch/lumberjack v2.0.0+incompatible
	github.com/nsf/termbox-go v0.0.0-20181027232701-60ab7e3d12ed // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/satori/go.uuid v1.2.0
	golang.org/x/lint v0.0.0-20181026193005-c67002cb31c3 // indirect
	golang.org/x/net v0.0.0-20181102091132-c10e9556a7bc
	golang.org/x/oauth2 v0.0.0-20181102170140-232e45548389 // indirect
	golang.org/x/sys v0.0.0-20181031143558-9b800f95dbbc // indirect
	golang.org/x/tools v0.0.0-20181102182153-619897c5a2e4 // indirect
	google.golang.org/appengine v1.3.0 // indirect
	google.golang.org/genproto v0.0.0-20181101192439-c830210a61df // indirect
	google.golang.org/grpc v1.16.0
	honnef.co/go/tools v0.0.0-20180920025451-e3ad64cb4ed3 // indirect
)
