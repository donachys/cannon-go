@echo off
set NAME=cannon-go
set CLI-NAME=cli-client
set SERVER-NAME=server

if exist make.bat (
	if [%1] == [] goto windows-cli

	if "%~1" == "windows-cli" (
		goto windows-cli
	) else if "%~1" == "windows-server" (
		goto windows-server
	) else if "%~1" == "test" (
		goto test
	)
)
echo Must run make.bat from Go src directory.
goto fail

:windows-cli
set GOOS=windows
set GOAARCH=amd64
go build -v -o ./bin/windows/amd64/%NAME%.exe ./cmd/%CLI-NAME%
goto end

:windows-server
set GOOS=windows
set GOAARCH=amd64
go build -v -o ./bin/windows/amd64/%SERVER-NAME%.exe ./cmd/%SERVER-NAME%
goto end

:test
go test -timeout 45s -race -cover $$(go list -f '{{.ImportPath}}' ./...)
goto end

:fail
set GOBUILDFAIL=1

:end