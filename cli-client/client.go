package cliclient

import (
	"flag"
	"fmt"
	"log"

	"github.com/natefinch/lumberjack"
	"google.golang.org/grpc"
)

const (
	defaultHost = "localhost"
	defaultPort = "55531"
)

var (
	conn *grpc.ClientConn
	host = flag.String("host", defaultHost, "The host url to connect to.")
	port = flag.String("port", defaultPort, "The host port to connect to.")
)

func Init() {
	flag.Parse()
	logSetup()
	grpcSetup()
}

func logSetup() {
	log.SetOutput(&lumberjack.Logger{
		Filename:   "./client.log",
		MaxSize:    1, // megabytes after which new file is created
		MaxBackups: 3,
		MaxAge:     28, //days
	})
}

func grpcSetup() {
	var err error
	serverAddr := fmt.Sprintf("%s:%s", *host, *port)
	conn, err = grpc.Dial(serverAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
}

func Teardown() {
	defer conn.Close()
}
