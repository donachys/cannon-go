package server

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net"

	"github.com/natefinch/lumberjack"
	cg "gitlab.com/donachys/cannon-go/cannon-go"
	"gitlab.com/donachys/cannon-go/game"
	"gitlab.com/donachys/cannon-go/gamestore"
	matcher "gitlab.com/donachys/cannon-go/matchmaker"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

const (
	defaultPort = "55531"
	prefixLen   = 128
)

type server struct{}

var (
	cgs  = &server{}
	port = flag.String("port", defaultPort, "The port to serve on.")
)

//Init initializes the server.
func Init() {
	flag.Parse()
	logSetup()
	initGRPC()
}
func logSetup() {
	log.SetOutput(&lumberjack.Logger{
		Filename:   "./server.log",
		MaxSize:    1, // megabytes after which new file is created
		MaxBackups: 3,
		MaxAge:     28, //days
	})
}

func initGRPC() {
	lis, err := net.Listen("tcp", fmt.Sprintf(":%s", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	cg.RegisterCannonGoServer(grpcServer, cgs)
	log.Println("Serving")
	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("failed to serve: %v", err)
	}
}

func (*server) Join(ctx context.Context, req *cg.JoinRequest) (*cg.JoinResponse, error) {
	log.Println("Join called with", req.UserName)
	matchedGame := matcher.GetCannonMatchMaker().Match(game.NewCannonPlayer(req.UserName))
	return &cg.JoinResponse{Secret: fmt.Sprintf("%s-%s", matchedGame.GameID(), req.UserName)}, nil
}
func (*server) StreamingPlay(stream cg.CannonGo_StreamingPlayServer) error {
	headers, ok := metadata.FromIncomingContext(stream.Context())
	if !ok {
		return errors.New("invalid metadata in context")
	}
	secrets, ok := headers["secret"]
	if !ok || len(secrets) < 1 || len(secrets[0]) < prefixLen+1{
		return errors.New("invalid metadata in context")
	}
	secret := secrets[0]
	gameID := secret[:prefixLen]
	userID := secret[prefixLen+1:]

	rWaitChan := make(chan struct{})
	sWaitChan := make(chan struct{})
	go func() {
		for {
			select {
			case <-rWaitChan:
				close(sWaitChan)
				return
			default:
				in, err := stream.Recv()
				if err == io.EOF {
					close(sWaitChan)
					return
				}
				if err != nil {
					log.Println("receive failed:", err)
					close(sWaitChan)
					return
				}
				log.Printf("got inc-message %v", in)
				if g, exists := gamestore.GetCannonGameStore().GetGame(gameID); exists {
					log.Printf("Handling request: %v in game: %v", in, g.GameID())
					g.HandleRequest(*in, userID)
				} else {
					// TODO: send some kind of error response for game non-existence.
				}

			}
		}

	}()

	go func() {
		updateChannel := make(chan *cg.StreamingPlayResponse, 5)
		if g, exists := gamestore.GetCannonGameStore().GetGame(gameID); exists {
			g.Players[userID].RegisterUpdateChannel(updateChannel)
		} else {
			// TODO: send some kind of error response for game non-existence.
		}
		for {
			select {
			case <-sWaitChan:
				close(rWaitChan)
				return
			case update := <-updateChannel:
				stream.Send(update)
			}
		}
	}()

	<-rWaitChan
	<-sWaitChan
	log.Println("exiting bidi stream for", userID)
	if g, exists := gamestore.GetCannonGameStore().GetGame(gameID); exists {
		log.Println("Calling Cancel for gameID:", gameID)
		g.Cancel()
	}
	return nil
}
