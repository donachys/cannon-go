package game

import (
	"math/rand"
	"testing"

	"github.com/google/go-cmp/cmp"
	cg "gitlab.com/donachys/cannon-go/cannon-go"
)

func TestAddPlayer(t *testing.T) {
	cases := []struct {
		given []*CannonPlayer
		want  []*CannonPlayer
	}{
		{
			given: []*CannonPlayer{NewCannonPlayer("test_player")},
			want:  []*CannonPlayer{NewCannonPlayer("test_player")},
		},
		{
			given: []*CannonPlayer{
				NewCannonPlayer("test_player1"),
				NewCannonPlayer("test_player2"),
				NewCannonPlayer("test_player3"),
			},
			want: []*CannonPlayer{
				NewCannonPlayer("test_player1"),
				NewCannonPlayer("test_player2"),
				NewCannonPlayer("test_player3"),
			},
		},
	}
	for _, c := range cases {
		cannonGame := NewCannonGame()
		for _, p := range c.given {
			cannonGame.AddPlayer(p)
		}
		for _, p := range c.want {
			if _, exists := cannonGame.Players[p.PlayerName()]; exists != true {
				t.Errorf("%v not present in %v", p.PlayerName(), cannonGame.Players)
			}
		}
	}
}

func TestGetGameID(t *testing.T) {
	testGame1 := NewCannonGame()
	cases := []struct {
		given *CannonGame
		want  string
	}{
		{
			given: testGame1,
			want:  testGame1.id,
		},
	}
	for _, c := range cases {
		if got := c.given.GameID(); got != c.want {
			t.Errorf("[%v != %v]", got, c.want)
		}

	}
}

func TestAllReady(t *testing.T) {
	cases := []struct {
		givenPlayers   []*CannonPlayer
		givenReadiness []bool
		want           bool
	}{
		{
			givenPlayers:   []*CannonPlayer{NewCannonPlayer("test_player")},
			givenReadiness: []bool{true},
			want:           true,
		},
		{
			givenPlayers: []*CannonPlayer{
				NewCannonPlayer("test_player1"),
				NewCannonPlayer("test_player2"),
				NewCannonPlayer("test_player3"),
			},
			givenReadiness: []bool{true, false, true},
			want:           false,
		},
	}
	for _, c := range cases {
		cannonGame := NewCannonGame()
		for i, p := range c.givenPlayers {
			cannonGame.AddPlayer(p)
			p.SetReady(c.givenReadiness[i])
		}

		if got := cannonGame.AllReady(); got != c.want {
			t.Errorf("%v != %v", c.want, got)
		}
	}
}
func TestGameState(t *testing.T) {
	rand.Seed(1)
	game1 := NewCannonGame()
	player1, player2 := NewCannonPlayer("bob"), NewCannonPlayer("alice")
	game1.AddPlayer(player1)
	game1.AddPlayer(player2)
	player1.SetReady(true)
	player2.SetReady(true)
	game1.start()
	cases := []struct {
		givenGame   *CannonGame
		givenPlayer *CannonPlayer
		want        *cg.GameState
	}{
		{
			givenGame:   game1,
			givenPlayer: player1,
			want: &cg.GameState{
				State: cg.GameState_ACTIVE,
				P0Base: &cg.Rect{
					TopLeft:  &cg.Point{X: 73, Y: 4},
					BotRight: &cg.Point{X: 78, Y: 9},
				},
				P1Base: &cg.Rect{
					TopLeft:  &cg.Point{X: 75, Y: 32},
					BotRight: &cg.Point{X: 80, Y: 37},
				},
				P0Misses: []*cg.Point{},
				P1Misses: []*cg.Point{},
				Cannon:   &cg.Point{X: 76, Y: 4},
			},
		},
	}
	for _, c := range cases {
		got := c.givenGame.GameState(c.givenPlayer)
		if diff := cmp.Diff(c.want, got); diff != "" {
			t.Errorf("%s: after GameState, differs: (-want +got)\n%s", got, diff)
		}
	}
}
