package game

import (
	"testing"

	cg "gitlab.com/donachys/cannon-go/cannon-go"
)

func TestPlayerName(t *testing.T) {
	cases := []struct {
		given *CannonPlayer
		want  string
	}{
		{
			given: NewCannonPlayer("test_name"),
			want:  "test_name",
		},
	}
	for _, c := range cases {
		if got := c.given.PlayerName(); got != c.want {
			t.Errorf("%v != %v", got, c.want)
		}
	}
}

func TestRegisterUpdateChannel(t *testing.T) {
	cases := []struct {
		givenChan   chan *cg.StreamingPlayResponse
		givenPlayer *CannonPlayer
		want        bool
	}{
		{
			givenChan:   make(chan *cg.StreamingPlayResponse, 5),
			givenPlayer: NewCannonPlayer("test_name"),
			want:        true,
		},
		{
			givenChan:   nil,
			givenPlayer: &CannonPlayer{playerName: "test_name"},
			want:        false,
		},
		{
			givenChan:   nil,
			givenPlayer: NewCannonPlayer("test_name"),
			want:        true,
		},
	}
	for _, c := range cases {
		p := c.givenPlayer
		if c.givenChan != nil && c.givenPlayer != nil {
			p.RegisterUpdateChannel(c.givenChan)
		}
		if got := p.updates != nil; got != c.want {
			t.Errorf("%v != %v", got, c.want)
		}
	}
}
